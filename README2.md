Files, Directories, Processes - Systems Programming Group Project

Project Description:

This was 1 of 2 final projects done for Systems Programming in the spring of 2017. 

The primary goal of the project was to get students comfortable working with files, directories, and processes using 
system calls in C. The final product is the "search" function, an incomplete implementation of the find function
used to search for particular files, directories, etc. 

Secondary goals of the project were to give students experience working together on complex code, splitting up
tasks, and building off of pre-existing code.
