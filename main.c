/* main.c */

#include "search.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/stat.h>
#include <unistd.h>

/* Functions */

void usage(const char *program_name, int status){
    fprintf(stderr, "Usage: %s PATH [OPTIONS] [EXPRESSION]\n", program_name);
    fprintf(stderr, "\nOptions:\n");
    fprintf(stderr, "    -executable     File is executable or directory is searchable to user\n");
    fprintf(stderr, "    -readable       File readable to user\n");
    fprintf(stderr, "    -writable       File is writable to user\n\n");
    fprintf(stderr, "    -type [f|d]     File is of type f for regular file or d for directory\n\n");
    fprintf(stderr, "    -empty          File or directory is empty\n\n");
    fprintf(stderr, "    -empty          File or directory is empty\n\n");
    fprintf(stderr, "    -name  pattern  Base of file name matches shell pattern\n");
    fprintf(stderr, "    -path  pattern  Path of file matches shell pattern\n\n");
    fprintf(stderr, "    -perm  mode     File's permission bits are exactly mode (octal)\n");
    fprintf(stderr, "    -newer file     File was modified more recently than file\n\n");
    fprintf(stderr, "    -uid   n        File's numeric user ID is n\n");
    fprintf(stderr, "    -gid   n        File's numeric group ID is n\n");
    fprintf(stderr, "\nExpressions:\n\n");
    fprintf(stderr, "    -print          Display file path (default)\n");
    fprintf(stderr, "    -exec cmd {} ;  Execute command on path\n");
    exit(status);
}

/* Main Execution */

int main(int argc, char *argv[]){ 
    //INITIALIZE SETTINGS
    Settings settings = {
        .access = 0,
        .uid = -1,
        .gid = -1,
    };
    

    // Parse options and execution
    settings.print  = true;

    
    const char * path = argv[1];
    int argind = 2; 
    while(argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-'){
        char *arg = argv[argind];

        if (!strcmp(arg, "-executable")) {
            settings.access |= X_OK;
        }
        else if (!strcmp(arg, "-readable")){
            settings.access |= R_OK;
        }
        else if (!strcmp(arg, "-writable")){
            settings.access |= W_OK;
        }
        else if (!strcmp(arg, "-type")){
            argind++;
            if (streq(argv[argind], "f") ) 
                settings.type = S_IFREG;       // Get type: file or directory
            
            if (!strcmp(argv[argind], "d")) 
                settings.type = S_IFDIR;
        }
        else if (!strcmp(arg, "-empty")){
            settings.empty = true;
        }
        else if (!strcmp(arg, "-name")){
            argind++;
            settings.name = argv[argind];
        }
        else if (!strcmp(arg, "-path")){
            argind++;
            settings.path = argv[argind];
        }
        else if (!strcmp(arg, "-perm")) {
            argind++;
            sscanf(argv[argind], "%o", &settings.perm);
        }
        else if (!strcmp(arg, "-newer")){
            argind++;
            char * n_path = argv[argind];
            time_t n_time = get_mtime(n_path); 
            settings.newer = n_time;
        }
        else if (!strcmp(arg, "-uid")){
            argind++;
            settings.uid = atoi(argv[argind]);
        }
        else if (streq(arg, "-gid")){
            argind++;
            settings.gid = atoi(argv[argind]);
        }
        else if (!strcmp(arg, "-print")){
            settings.print = true;
        } 
        else if (!strcmp(arg, "-exec")){
            settings.print = false;
            argind++;
            int cp_argind  = argind;
           
            while (argind < argc) {
                argind++;
                if (streq(argv[argind], ";")) break;
            }
            //Get size of the array of commands 
            settings.exec_argc = argc - cp_argind - 1;
         
           //Save commands into an array
           settings.exec_argv = malloc(sizeof(char *) * settings.exec_argc);
           for (int i = 0; i < settings.exec_argc; i++) {
                
                int length = strlen(argv[cp_argind]);
                
                settings.exec_argv[i] = malloc((length + 1) * sizeof(char));
           
                int j;

                for (j = 0; j < length; j++) {
                    settings.exec_argv[i][j] = argv[cp_argind][j];
                }
                cp_argind++;
           }

        } 
        argind++;
    }
    
    if (!filter(path, &settings)) {
        execute(path, &settings);
    }
    search(path, &settings); 
    /*Check for no arguments*/
return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
