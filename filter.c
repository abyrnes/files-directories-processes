/* filter.c */

#include "search.h"

#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include<libgen.h>
#include <dirent.h>
#include <fnmatch.h>
#include <sys/stat.h>
#include <unistd.h>
#define st_mtime st_mtim.tv_sec 

/**
 * Test whether or not the given path should be filtered out of the output.
 * @param   path        Path to file or directory.
 * @param   settings    Settings structure.
 * @return  Whether or not the path should be excluded from the output (true to
 * exclude, false to include).
 */
bool        filter(const char *path, const Settings *settings) {
   struct stat buffer;
    lstat(path, &buffer);
   // printf("JEYYY");
    if (lstat(path, &buffer) < 0) {
        printf("Error opening file");
        return true;
    }

    /* Checking for each part*/
    if  (settings->access) {
        if (access(path, settings->access) != 0)
            return true;
        
    }

    if(settings->type) {           
        if (settings->type != (buffer.st_mode & S_IFMT))     
            return true;
    }
        

    if(settings->empty) { 
        switch(buffer.st_mode & S_IFMT) {
            case (S_IFREG):
                if (buffer.st_size > 0) 
                    return true;
                
                break;
        case (S_IFDIR):
            if (!is_directory_empty(path)) 
                return true;
            break;
            
        default:
            return true;
            break;  
       } 
    }
   
   if (settings->name) {
      char * new_path = (char *) path;
      char * base_path = basename(new_path);
        if (fnmatch(settings->name, base_path, FNM_PATHNAME)) {
            return true;
        }
   }

   if (settings->path) {
        if (fnmatch(settings->path, (char *)path, FNM_PERIOD))
            return true;
   }

   if (settings->perm) {
        if (settings->perm != (buffer.st_mode & (S_IRWXU | S_IRWXG | S_IRWXO))){
            return true;
        }
   }

   if (settings->newer) {
        if (buffer.st_mtime <= settings->newer){
            return true; 
        } 
   }

   if (settings->uid >= 0 && buffer.st_uid != settings->uid) {
       return true; 
        
   }

   if (settings->gid >= 0 && buffer.st_gid != settings->gid) {
            return true;
        
    }

 //   printf("In FILTER\n");
return false;

}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
