Files, Directories, Processes - Systems Programming Group Project
=================================================================
Project Description 8/8/17
--------------------------

This was 1 of 2 final projects done for Systems Programming in the spring of 2017. 

The primary goal of the project was to get students comfortable working with files, directories, and processes using 
system calls in C. The final product is the "search" function, an incomplete implementation of the find function
used to search for particular files, directories, etc. 

Secondary goals of the project were to give students experience working together on complex code, splitting up
tasks, and building off of pre-existing code.



Project 01 - README (Original README)
====================================

Members
-------

-Margaret (Maggie) West (mwest6@nd.edu)
 
-Melissa Pech-Figueroa (mpechfig@nd.edu)
 
-Amy J Byrnes (abyrnes@nd.edu) 

Questions
---------
1. How did you handle parsing the command line options?
2. How did you walk the directory tree?
3. How did you determine whether or not to print a filesystem object's path?

Responses
---------
1. To parse the command line arguements, we created a while loop that continues while there are less command line arguements that ones we have reached with argind, AND the command line arguement reached by argind has length, AND that there is a dash in front of each argument, to signal the arg is a flag. In this while loop, we check if each of the accepted flags of the Settings structure are used, and if they are used, we assign values to that variable in the struct.  
2. First we opened the dircetory specified by path. If no such directory it would return an exit failure. Then we kept reading entries as long it was not equal to NULL. Then we skipped over the parent and current directories. if filter returned false, then it would execute, and if itw as a directory, we would save the new path, and pass it using recursion to search again. This allowed us to look into
every single directory and subdirectories. 
3. By default, an object's path was printed unless the -exec expression was specified. During execution, if the -print option was specified, even if the -exec option was also specifed, the path was printed. Checking whether an expression was specified was done by checking whether settings->print and settings->exec_argc existed or were non-zero quantities

Script
-------
Search uses many more system calls than find, but find uses more unique system calls than search. Some surprises were how many times lstat is called in search and the relatively small number of times search calls open compared to find.

Contributions
-------------
Maggie: 
1. worked on filter.c
	empty
2. worked on main.c 
3. worked on execute.c
4. Debugging

Melissa:
1. Makefile 
2. utilities.c 
3. search.c 
4. worked on main.c 
5. execute.c
6. filter.c
	type, name, path, uid, gid, empty
7. Debugging w/ test_search.sh 


Amy:
1. work on execute.c 
2. Debugging w/ test_search.sh
3. worked on main.c
4. worked on filter.c
	newer, perm
5. Script with python/shell script
